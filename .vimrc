
" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2011 Apr 15
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

set laststatus=2   " всегда показывать строку статуса
"set statusline=%f%m%r%h%w\ %y\ enc:%{&enc}\ ff:%{&ff}\ fenc:%{&fenc}%=(ch:%3b\ hex:%2B)\ col:%2c\ line:%2l/%L\ [%2p%%]
" настройки Vim-Airline
let g:airline_theme='dark'
let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#formatter = 'unique_tail'
" let g:airline_section_b = '%{strftime("%c")}'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'
let g:airline_linecolumn_prefix = '¶ '
let g:airline_fugitive_prefix = '⎇ '
let g:airline_paste_symbol = 'ρ'

" Настройка Unite
" Автоматический insert mode
let g:unite_enable_start_insert = 1

" Отображаем Unite в нижней части экрана
let g:unite_split_rule = "botright"

" Отключаем замену статус строки
let g:unite_force_overwrite_statusline = 0

" Размер окна 
let g:unite_winheight = 10

" Красивые стрелочки
let g:unite_candidate_icon="▶"

" Свое меню
let g:unite_source_menu_menus = {}
let g:unite_source_menu_menus.mymenu = {
            \     'description' : 'My Unite menu',
            \ }
let g:unite_source_menu_menus.mymenu.candidates = {
            \   'mru&buffer'      : 'Unite buffer file_mru',
            \   'tag'      : 'Unite tag',
            \   'file'      : 'Unite file',
            \   'file_rec'      : 'Unite file_rec',
            \   'file_rec/async'      : 'Unite file_rec/async',
            \   'find'      : 'Unite find',
            \   'grep'      : 'Unite grep',
            \   'register'      : 'Unite register',
            \   'bookmark'      : 'Unite bookmark',
            \   'output'      : 'Unite output',
            \ }
function g:unite_source_menu_menus.mymenu.map(key, value)
		return {
			\   'word' : a:key, 'kind' : 'command',
			\   'action__command' : a:value,
		 	\ }
endfunction

" END

set noswapfile
set number
set tabstop=4

let &showbreak = '↳ '
set wrap
"set cpo=79
" Автоперенос по словам
"set tw=79
"set formatoptions+=t

" Кодировка терминала, должна совпадать с той, которая используется для вывода
" в терминал
set termencoding=utf-8
set encoding=utf8

" возможные кодировки файлов и последовательность определения.
set fileencodings=utf8,cp1251

set t_Co=256
colorscheme desert256

"if exists('+colorcolumn')
"	highlight ColorColumn ctermbg=235 guibg=#2c2d27
"	highlight CursorLine ctermbg=235 guibg=#2c2d27
"	highlight CursorColumn ctermbg=235 guibg=#2c2d27
"	let &colorcolumn=join(range(81,999),",")
"else
"	autocmd BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
"end
set colorcolumn=80
highlight ColorColumn ctermbg=darkgray

" автоотступы для новых строк
set ai
" отступы в стиле Си
set cin

set mouse=a "Включить поддержку мыши
autocmd VimEnter * NERDTree

set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

" autocmd VimEnter * if &filetype !=# 'gitcommit' | NERDTree | wincmd p | endif

function! NTFinderP()
    "" Check if NERDTree is open
    if exists("t:NERDTreeBufName")
        let s:ntree = bufwinnr(t:NERDTreeBufName)
    else
        let s:ntree = -1
    endif
    if (s:ntree != -1)
        "" If NERDTree is open, close it.
        :NERDTreeClose
    else
        "" Try to open a :Rtree for the rails project
        if exists(":Rtree")
            "" Open Rtree (using rails plugin, it opens in project dir)
            :Rtree
        else
            "" Open NERDTree in the file path
            :NERDTreeFind
        endif
    endif
endfunction
autocmd VimEnter * wincmd p

"############################## Подсветка #################################
augroup project
	autocmd!
	autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

"############################## NeoBundle #################################
if has('vim_starting')
  set nocompatible               " Be iMproved

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

NeoBundle 'bling/vim-airline'
" NeoBundle 'Shougo/vimproc.vim'
NeoBundle 'Shougo/unite.vim'
" NeoBundle 'tpope/vim-fugitive'
NeoBundle 'motemen/git-vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'vim-scripts/tComment'
NeoBundle 'mileszs/ack.vim'
" NeoBundle 'dahu/vim-asciidoc'

"############################## Bundle ####################################
"set nocompatible
"filetype off  " обязательно!

"set rtp+=~/.vim/bundle/vundle/

"call vundle#rc()

"filetype plugin indent on     " обязательно!

"репозитории на github
"Bundle 'tpope/vim-fugitive'
"Bundle 'lokaltog/vim-easymotion'
"Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
"Bundle 'bling/vim-airline'
"Bundle 'motemen/git-vim'

"репозитории vim/scripts
"Bundle 'L9'
"Bundle 'FuzzyFinder'
"Bundle 'rails.vim'

"git репозитории (не на github)
"Bundle 'git://git.wincent.com/command-t.git'


"############################## Key maps ##################################
" Сохранение буфера в файл
nmap <c-s> :w<CR>
vmap <c-s> <Esc><c-s>gv
imap <c-s> <Esc><c-s>

" Сохранение буфера по <F2>
nmap <F2> :update<CR>
vmap <F2> <Esc><F2>gv
imap <F2> <c-o><F2>

" Поиск ack-grep по <F3>
nmap <F3> <Esc>:Ack --ignore-file=match:tags ''<Left>
vmap <F3> <Esc>:Ack --ignore-file=match:tags ''<Left>
imap <F3> <Esc><Esc>:Ack --ignore-file=match:tags ''<Left>

" Просмотр списка буферов по <F4>
nmap <F4> <Esc>:Unite buffer<CR>
vmap <F4> <Esc>:Unite buffer<CR>
imap <F4> <Esc><Esc>:Unite buffer<CR>

" Выход по <F10>
nmap <F10> <Esc>:q<CR>
vmap <F10> <Esc>:q<CR>
imap <F10> <Esc><Esc>:q<CR>

" Выход без сохранения по <F12>
nmap <F12> <Esc>:q!<CR>
vmap <F12> <Esc>:q!<CR>
imap <F12> <Esc><Esc>:q!<CR>

" предыдущий буфер
map <F5> :bp<CR>
vmap <F5> <Esc>:bp<CR>i
imap <F5> <Esc>:bp<CR>i

" следующий буфер
map <F6> :bn<CR>
vmap <F6> <Esc>:bn<CR>i
imap <F6> <Esc>:bn<CR>i

" Переключение табов (вкладок) с помощью SHIFT+TAB и CTRL+TAB
map <S-TAB> :tabprevious<CR>
nmap <S-TAB> :tabprevious<CR>
imap <S-TAB> <Esc>:tabprevious<CR>i
map <C-TAB> :tabnext<CR>
nmap <C-TAB> :tabnext<CR>
imap <C-TAB> <Esc>:tabnext<CR>i

" Открытие\закрытие новой вкладки по CTRL+T и CTRL+W
" nmap <C-t> :tabnew<CR>
" imap <C-t> <Esc>:tabnew<CR>a
" nmap <C-w> :tabclose<CR>
" imap <C-w> <Esc>:tabclose<CR>


"############################## AsciiDoc ##################################
" http://www.methods.co.nz/asciidoc/chunked/ape.html

" Use bold bright fonts.
" set background=dark

" Show tabs and trailing characters.
"set listchars=tab:»·,trail:·,eol:¬
set listchars=tab:»·,trail:·
set list

" Reformat paragraphs and list.
nnoremap <Leader>r gq}

" Delete trailing white space and Dos-returns and to expand tabs to spaces.
nnoremap <Leader>t :set et<CR>:retab!<CR>:%s/[\r \t]\+$//<CR>

autocmd BufRead,BufNewFile *.txt,*.asciidoc,README,TODO,CHANGELOG,NOTES,ABOUT
        \ setlocal autoindent expandtab tabstop=8 softtabstop=2 shiftwidth=2 filetype=asciidoc
        \ textwidth=70 wrap formatoptions=tcqn
        \ formatlistpat=^\\s*\\d\\+\\.\\s\\+\\\\|^\\s*<\\d\\+>\\s\\+\\\\|^\\s*[a-zA-Z.]\\.\\s\\+\\\\|^\\s*[ivxIVX]\\+\\.\\s\\+
        \ comments=s1:/*,ex:*/,://,b:#,:%,:XCOMM,fb:-,fb:*,fb:+,fb:.,fb:>
